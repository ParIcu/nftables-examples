```shell
iptables -t nat -A POSTROUTING -j MASQUERADE
nft add rule ip nat POSTROUTING counter masquerade
# nftables.conf
counter masquerade

iptables -t nat -A POSTROUTING -p tcp -j MASQUERADE --to-ports 10
nft add rule ip nat POSTROUTING meta l4proto tcp counter masquerade to :10
# nftables.conf
meta l4proto tcp counter masquerade to :10  

iptables -t nat -A POSTROUTING -p tcp -j MASQUERADE --to-ports 10-20 --random
nft add rule ip nat POSTROUTING meta l4proto tcp counter masquerade to :10-20 random
# # nftables.conf
meta l4proto tcp counter masquerade to :10-20 random 

```
